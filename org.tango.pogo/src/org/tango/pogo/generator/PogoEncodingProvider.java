
package org.tango.pogo.generator;

import org.eclipse.emf.common.util.URI;
import org.eclipse.xtext.parser.IEncodingProvider;

public class PogoEncodingProvider implements IEncodingProvider
    {
        public String getEncoding(URI uri)
        {
            return "UTF-8";
        }
    }
